//Look into what is going on here..
let hashMap = {};

//First we are creating a function to 'sanitize' the string: 
//1)setting each character to lowercase
//2)alphabetizing it; 3)splitting the string into an array of each letter
//4)sorting that array of letters into alphabetized list; 
//5) and finally 'joining' that sorted array of letters back together into one word
function alphabetize(a) {
    return a.toLowerCase().split("").sort().join("").trim();
}

function loadAnagramCharMap() {
    let charmap = words.reduce((pre, cur, index, arr) => {
        let key = alphabetize(cur);
        if (!pre[key]) pre[key] = [cur];
        else pre[key].push(cur)
        return pre;
    }, {})

    return charmap
}

function getAnagramsOf(string) {
    let key = alphabetize(string);
    hashmap = loadAnagramCharMap();

    return hashmap[key]
}

const button = document.getElementById("findButton");
button.onclick = function() {
    let typedText = document.getElementById("input").value;
    let anagrams = getAnagramsOf(typedText);

    document.getElementById("output").textContent = anagrams
}